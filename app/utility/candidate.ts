import * as _ from 'lodash';
import { calculateData } from './premium';

export interface ICandidate {
  name: string;
  age: number;
  gender: string;
  smoker: boolean;
  email: string;
  height: number;
  weight: number;
  health: string[];
  alcohol: number;
  postalcode: string;
  policyrequested: number;
}

export interface ICandidateResponse {
  name: string;
  BMI: number;
  score: number;
  premium: number;
  coveragePrice: number;
}

export function parseData(data: any[]): ICandidateResponse[] {
  const candidates = data.map((dataRow): ICandidate => {
    const parsedData = {
      health: parseHealth(dataRow.health),
      smoker: parseSmoker(dataRow.smoker),
    };

    return _.merge({}, dataRow, parsedData);
  });

  return candidates.map((candidate: ICandidate): ICandidateResponse => calculateData(candidate));
}

export function parseHealth(health: string): string[] {
  return health
    .toLowerCase()
    .replace('[', '')
    .replace(']', '')
    .split(',');
}

export function parseSmoker(smoker: string): boolean {
  return smoker === 'S';
}

export function calculateBMI(height: number, weight: number): number {
  const BMI = weight / Math.pow(height / 100, 2);
  return _.round(BMI, 1);
}
