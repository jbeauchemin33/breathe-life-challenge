import * as fs from 'fs';
import * as Parser from 'csv-parse';
import * as Stringify from 'csv-stringify';

const FILE_PATH = `${__dirname}/../`;

export async function importCSV(): Promise<any> {
  return new Promise((resolve, reject) => {
    const parser = Parser({ columns: true }, (err, data) => {
      // TODO: use stream instead to parse the file in case we receive a huge file, to prevent node from running out of memory
      if (err) return reject(err);
      resolve(data);
    });

    fs.createReadStream(`${FILE_PATH}input.csv`).pipe(parser);
  });
}

export async function exportCSV(data) {
  const columns = {
    name: 'name',
    BMI: 'BMI',
    score: 'score',
    premium: 'premium',
    coveragePrice: 'coveragePrice',
  };

  return new Promise((resolve, reject) => {
    Stringify(data, { columns, header: true }, (err, output) => {
      if (err) return reject(err);

      fs.writeFile(`${FILE_PATH}output.csv`, output, (err) => {
        if (err) throw err;
        resolve();
      });
    });
  });
}
