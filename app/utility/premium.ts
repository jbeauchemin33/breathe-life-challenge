import * as _ from 'lodash';
import { calculateBMI, ICandidate, ICandidateResponse } from './candidate';

function clause1(candidate: ICandidate): number {
  const depression = candidate.health.includes('depression');
  const anxiety = candidate.health.includes('anxiety');
  const BMI = calculateBMI(candidate.height, candidate.weight);
  const risky = (depression || anxiety) && BMI < 18.5;
  return risky ? 15 : 0;
}

function clause2(candidate: ICandidate): number {
  const surgery = candidate.health.includes('surgery');
  const BMI = calculateBMI(candidate.height, candidate.weight);
  const risky = surgery && candidate.smoker && BMI > 25 && candidate.alcohol > 10;
  return risky ? 25 : 0;
}

function clause3(candidate: ICandidate): number {
  const hearthProblem = candidate.health.includes('heart');
  const BMI = calculateBMI(candidate.height, candidate.weight);
  const risky = hearthProblem && BMI > 30 && candidate.alcohol > 25;
  return risky ? 30 : 0;
}

function calculateScore(candidate: ICandidate): number {
  return clause1(candidate) + clause2(candidate) + clause3(candidate);
}

function calculatePremium(score: number): number {
  if (score === 0) return 1;
  if (score > 75) return 1.15;
  if (score > 100) return 1.25;
  return null;
}

function calculateCoverageRate(age: number, smoker: boolean): number {
  // TODO: is 40 years old considered young or old?
  const young = age >= 18 && age < 40;
  const old = age >= 40 && age <= 60;

  if (young && !smoker) return 0.10;
  if (young && smoker) return 0.25;
  if (old && !smoker) return 0.30;
  if (old && smoker) return 0.55;

  return 0;
}

// TODO: replace any type
export function calculateData(candidate: ICandidate): ICandidateResponse {
  const score = calculateScore(candidate);
  const premium = calculatePremium(score);
  const coverageRate = calculateCoverageRate(candidate.age, candidate.smoker);
  const coveragePrice = _.round((premium * coverageRate * candidate.policyrequested) / 1000, 2);
  const BMI = calculateBMI(candidate.height, candidate.weight);

  return {
    BMI,
    score,
    premium,
    coveragePrice,
    name: candidate.name,
  };
}
