import { importCSV, exportCSV } from './utility/csv';
import { parseData } from './utility/candidate';

async function start() {
  await importCSV()
    .then(parseData)
    .then(exportCSV)
    .catch((err) => {
      console.error('error processing candidates', err);
      process.exit(1);
    });
}

start();
