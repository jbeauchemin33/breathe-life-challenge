# Breathe Life Challenge

> Janic Beauchemin

## Build Setup

``` bash
# Use the right node version
$ nvm use

# Install dependencies
$ npm install

# Run the project
$ npm start

# output will be in app/output.csv
```
